package com.github.axet.torrentclient.activities;

import android.app.KeyguardManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.Adapter;
import android.widget.HeaderViewListAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.github.axet.androidlibrary.app.AlarmManager;
import com.github.axet.androidlibrary.services.StorageProvider;
import com.github.axet.androidlibrary.services.WifiReceiver;
import com.github.axet.androidlibrary.widgets.AboutPreferenceCompat;
import com.github.axet.androidlibrary.widgets.AppCompatThemeActivity;
import com.github.axet.androidlibrary.widgets.HeaderGridView;
import com.github.axet.androidlibrary.widgets.OpenChoicer;
import com.github.axet.androidlibrary.widgets.OpenFileDialog;
import com.github.axet.androidlibrary.widgets.OptimizationPreferenceCompat;
import com.github.axet.androidlibrary.widgets.ScreenlockPreference;
import com.github.axet.androidlibrary.widgets.SearchView;
import com.github.axet.androidlibrary.widgets.ThemeUtils;
import com.github.axet.androidlibrary.widgets.Toast;
import com.github.axet.torrentclient.R;
import com.github.axet.torrentclient.app.EnginesManager;
import com.github.axet.torrentclient.app.MainApplication;
import com.github.axet.torrentclient.app.MetainfoBuilder;
import com.github.axet.torrentclient.app.Storage;
import com.github.axet.torrentclient.app.TorrentPlayer;
import com.github.axet.torrentclient.dialogs.AddDialogFragment;
import com.github.axet.torrentclient.dialogs.CreateDialogFragment;
import com.github.axet.torrentclient.dialogs.OpenIntentDialogFragment;
import com.github.axet.torrentclient.dialogs.RatesDialogFragment;
import com.github.axet.torrentclient.dialogs.TorrentDialogFragment;
import com.github.axet.torrentclient.navigators.Torrents;
import com.github.axet.torrentclient.services.TorrentContentProvider;
import com.github.axet.torrentclient.widgets.Drawer;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import libtorrent.Libtorrent;

public class MainActivity extends AppCompatThemeActivity implements AbsListView.OnScrollListener,
        DialogInterface.OnDismissListener, SharedPreferences.OnSharedPreferenceChangeListener {
    public final static String TAG = MainActivity.class.getSimpleName();

    public static final int RESULT_ADD_ENGINE = 2;
    public static final int RESULT_ADD_TORRENT = 4;
    public static final int RESULT_CREATE_TORRENT = 5;

    public static final String ACTION_SHUTDOWN = MainActivity.class.getCanonicalName() + ".SHUTDOWN";

    public int scrollState;

    Runnable refresh;
    Runnable refreshUI;
    TorrentFragmentInterface dialog;

    Torrents torrents;
    ProgressBar progress;
    HeaderGridView list;
    View empty;
    Handler handler = new Handler();

    Drawer drawer;

    // not delared locally - used from two places
    FloatingActionsMenu fab;
    FloatingActionButton create;
    FloatingActionButton add;

    TextView freespace;
    ImageView turtle;

    // delayedIntent delayedIntent
    boolean delayedIntent;
    Runnable delayedInit;

    ScreenReceiver screenreceiver;

    EnginesManager engies;

    public long playerTorrent;
    TorrentPlayer.Receiver playerReceiver;
    View fab_panel;
    android.support.design.widget.FloatingActionButton fab_play;
    View fab_stop;
    TextView fab_status;
    Storage storage;
    OpenChoicer choicer;

    String lastSearch;

    public static void showLocked(Window w) {
        AppCompatThemeActivity.showLocked(w);
        ScreenlockPreference.showLocked(w, MainApplication.PREFERENCE_SCREENLOCK);
    }

    public static void showDialogLocked(Window w) {
        AppCompatThemeActivity.showDialogLocked(w);
        ScreenlockPreference.showLocked(w, MainApplication.PREFERENCE_SCREENLOCK);
    }

    public static void startActivity(Context context) {
        Intent i = new Intent(context, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(i);
    }

    public static void shutdown(Context context) {
        Intent i = new Intent(context, MainActivity.class);
        i.setAction(ACTION_SHUTDOWN);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(i);
    }

    public static Intent openFolderIntent(Context context, Storage.Torrent t) {
        return TorrentContentProvider.getProvider().openFolderIntent(context, t.path);
    }

    public static boolean isCallable(Context context, Intent intent) {
        return StorageProvider.isFolderCallable(context, intent, TorrentContentProvider.getProvider().getAuthority());
    }

    public interface TorrentFragmentInterface {
        void update();

        void close();
    }

    public interface NavigatorInterface {
        void install(HeaderGridView list);

        void search(String q);

        void searchClose();

        boolean onCreateOptionsMenu(Menu menu);

        boolean onOptionsItemSelected(MenuItem item);

        void remove(HeaderGridView list);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
    }

    public void checkTorrent(long t) {
        if (Libtorrent.torrentStatus(t) == Libtorrent.StatusChecking) {
            Libtorrent.stopTorrent(t);
            Toast.makeText(this, R.string.stop_checking, Toast.LENGTH_SHORT).show();
            return;
        }
        Storage.Torrent tt = storage.find(t);
        if (tt == null) // new created temporary torrents has no storage
            Libtorrent.checkTorrent(t);
        else
            tt.check();
        Toast.makeText(this, R.string.start_checking, Toast.LENGTH_SHORT).show();
    }

    public void renameDialog(final Long f) {
        final OpenFileDialog.EditTextDialog e = new OpenFileDialog.EditTextDialog(this);
        e.setTitle(getString(R.string.rename_torrent));
        e.setText(Libtorrent.torrentName(f));
        e.setPositiveButton(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String name = e.getText();
                // clear slashes
                name = new File(name).getName();
                if (name.isEmpty())
                    return;
                Libtorrent.torrentRename(f, name);
                torrents.notifyDataSetChanged();
            }
        });
        AlertDialog d = e.create();

        Window w = d.getWindow();
        showDialogLocked(w);

        d.show();
    }

    public MainApplication getApp() {
        return (MainApplication) getApplication();
    }

    @Override
    public int getAppTheme() {
        return MainApplication.getTheme(this, R.style.AppThemeLight_NoActionBar, R.style.AppThemeDark_NoActionBar);
    }

    public ListAdapter getAdapter() {
        ListAdapter a = list.getAdapter();
        if (a != null && a instanceof HeaderGridView.HeaderViewGridAdapter) {
            a = ((HeaderGridView.HeaderViewGridAdapter) a).getWrappedAdapter();
        }
        if (a != null && a instanceof HeaderViewListAdapter) {
            a = ((HeaderViewListAdapter) a).getWrappedAdapter();
        }
        return a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");

        setContentView(R.layout.app_bar_main);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (OptimizationPreferenceCompat.needKillWarning(this, MainApplication.PREFERENCE_NEXT))
            OptimizationPreferenceCompat.buildKilledWarning(this, true, MainApplication.PREFERENCE_OPTIMIZATION).show();

        drawer = new Drawer(this, toolbar);

        engies = new EnginesManager(this);

        fab = (FloatingActionsMenu) findViewById(R.id.fab);

        create = (FloatingActionButton) findViewById(R.id.torrent_create_button);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String path = "";

                final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);

                if (path == null || path.isEmpty()) {
                    path = MainApplication.getPreferenceLastPath(MainActivity.this);
                }

                Uri u = Uri.fromFile(new File(path));

                choicer = new OpenChoicer(OpenFileDialog.DIALOG_TYPE.BOOTH, true) {
                    @Override
                    public void onResult(Uri uri) {
                        File p = new File(uri.getPath());
                        shared.edit().putString(MainApplication.PREFERENCE_LAST_PATH, p.getParent()).commit();

                        String path = p.getPath();
                        File parent = new File(path).getParentFile();
                        if (parent == null) {
                            parent = new File("/");
                            path = ".";
                        }
                        try {
                            File f = new File(path);
                            path = f.getCanonicalPath(); // resolve symlink
                        } catch (IOException e) {
                            // ignore
                        }

                        final Uri pp = Uri.fromFile(parent);

                        createTorrent(pp, Uri.fromFile(new File(path)));
                    }
                };
                choicer.setPermissionsDialog(MainActivity.this, Storage.PERMISSIONS_RW, RESULT_CREATE_TORRENT);
                choicer.show(u);

                fab.collapse();
            }
        });

        add = (FloatingActionButton) findViewById(R.id.torrent_add_button);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                Uri u = storage.getStoragePath();
                String path = MainApplication.getPreferenceLastPath(MainActivity.this);
                if (path.startsWith(ContentResolver.SCHEME_FILE))
                    u = Uri.fromFile(new File(path));
                choicer = new OpenChoicer(OpenFileDialog.DIALOG_TYPE.FILE_DIALOG, false) {
                    @Override
                    public void onResult(Uri uri) {
                        String s = uri.getScheme();
                        byte[] buf;
                        boolean show = shared.getBoolean(MainApplication.PREFERENCE_DIALOG, false);
                        Uri u = storage.getStoragePath();
                        if (s.equals(ContentResolver.SCHEME_FILE)) { // we open torrent on top file
                            File f = new File(uri.getPath());
                            File p = f.getParentFile();
                            shared.edit().putString(MainApplication.PREFERENCE_LAST_PATH, p.getAbsolutePath()).commit();
                            try {
                                buf = FileUtils.readFileToByteArray(f);
                                if (!f.canWrite())
                                    show = true;
                                if (p.canWrite()) { // we adding file from folder with RW access, use storage current folder
                                    u = Uri.fromFile(p);
                                }
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                        } else if (s.equals(ContentResolver.SCHEME_CONTENT)) { // we open torrent at default location
                            try {
                                ContentResolver resolver = getContentResolver();
                                buf = IOUtils.toByteArray(resolver.openInputStream(uri));
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                        } else {
                            throw new Storage.UnknownUri();
                        }
                        addTorrentFromBytes(u, buf, show);
                    }
                };
                choicer.setPermissionsDialog(MainActivity.this, Storage.PERMISSIONS_RW, RESULT_ADD_TORRENT);
                choicer.setStorageAccessFramework(MainActivity.this, RESULT_ADD_TORRENT);
                choicer.show(u);
                fab.collapse();
            }
        });

        FloatingActionButton magnet = (FloatingActionButton) findViewById(R.id.torrent_magnet_button);
        magnet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final OpenFileDialog.EditTextDialog f = new OpenFileDialog.EditTextDialog(MainActivity.this);
                f.setTitle(getString(R.string.add_magnet));
                f.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String ff = f.getText();
                        addMagnet(ff);
                    }
                });
                AlertDialog d = f.create();

                Window w = d.getWindow();
                showDialogLocked(w);

                d.show();
                fab.collapse();
            }
        });

        showLocked(getWindow());

        progress = (ProgressBar) findViewById(R.id.progress);

        list = (HeaderGridView) findViewById(R.id.list);
        list.setPadding(list.getPaddingLeft(), list.getPaddingTop(), list.getRight(), list.getPaddingBottom());
        empty = findViewById(R.id.empty_list);

        fab.setVisibility(View.GONE);
        empty.setVisibility(View.GONE);
        list.setVisibility(View.GONE);
        fab.setVisibility(View.GONE);

        freespace = (TextView) findViewById(R.id.space_left);
        turtle = (ImageView) findViewById(R.id.turtle);

        freespace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRates();
            }
        });
        turtle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                boolean b = shared.getBoolean(MainApplication.PREFERENCE_SPEEDLIMIT, false);
                b = !b;
                boolean bb = shared.getInt(MainApplication.PREFERENCE_UPLOAD, -1) == -1 && shared.getInt(MainApplication.PREFERENCE_DOWNLOAD, -1) == -1;
                if (b && bb) {
                    showRates();
                    return;
                }
                SharedPreferences.Editor edit = shared.edit();
                edit.putBoolean(MainApplication.PREFERENCE_SPEEDLIMIT, b);
                edit.commit();
            }
        });

        screenreceiver = new ScreenReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                super.onReceive(context, intent);
                if (drawer.isDrawerOpen()) {
                    drawer.closeDrawer();
                }
            }
        };
        screenreceiver.registerReceiver(this);

        final MainApplication app = getApp();

        delayedIntent = getIntent() != null;

        // UI thread
        delayedInit = new Runnable() {
            @Override
            public void run() {
                storage = app.getStorage();

                progress.setVisibility(View.GONE);
                list.setVisibility(View.VISIBLE);
                fab.setVisibility(View.VISIBLE);

                invalidateOptionsMenu();

                list.setOnScrollListener(MainActivity.this);
                list.setEmptyView(empty);

                final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                shared.registerOnSharedPreferenceChangeListener(MainActivity.this);

                torrents = new Torrents(MainActivity.this, list);
                show(torrents);
                engies.load();

                if (Build.VERSION.SDK_INT >= 21) {
                    migrateLocalStorage();
                } else {
                    if (Storage.permitted(MainActivity.this, Storage.PERMISSIONS_RW)) {
                        migrateLocalStorage();
                    } else {
                        // with no permission we can't choise files to 'torrent', or select downloaded torrent
                        // file, since we have no persmission to user files.
                        create.setVisibility(View.GONE);
                        add.setVisibility(View.GONE);
                    }
                }

                // update unread icon after torrents created
                drawer.updateUnread();

                drawer.updateManager();

                if (app.player != null) {
                    app.player.setContext(MainActivity.this);
                    app.player.notifyProgress(playerReceiver);
                }

                if (delayedIntent) {
                    openIntent(getIntent());
                    delayedIntent = false;
                }
            }
        };

        updateHeader(new Storage(this));

        app.createThread(new Runnable() {
            @Override
            public void run() {
                if (isFinishing())
                    return;
                if (delayedInit != null) {
                    delayedInit.run();
                    delayedInit = null;
                }
            }
        });


        fab_panel = findViewById(R.id.fab_panel);
        fab_status = (TextView) findViewById(R.id.fab_status);
        fab_play = (android.support.design.widget.FloatingActionButton) findViewById(R.id.fab_play);
        fab_stop = findViewById(R.id.fab_stop);
        fab_panel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null)
                    return;
                Storage.Torrent t = storage.find(playerTorrent);
                if (t == null) {
                    Toast.makeText(MainActivity.this, R.string.not_permitted, Toast.LENGTH_SHORT).show(); // deleted while playing
                    return;
                }
                TorrentDialogFragment d = TorrentDialogFragment.create(playerTorrent);
                dialog = d;
                d.show(getSupportFragmentManager(), "");
            }
        });
        fab_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                app.player.pause();
            }
        });
        fab_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                app.playerStop();
                playerStop();
            }
        });
        fab_panel.setVisibility(View.GONE);

        playerReceiver = new TorrentPlayer.Receiver(this) {
            @Override
            public void onReceive(Context context, Intent intent) {
                String a = intent.getAction();
                if (a.equals(TorrentPlayer.PLAYER_PROGRESS)) {
                    fab_panel.setVisibility(View.VISIBLE);
                    playerTorrent = intent.getLongExtra("t", -1);
                    long pos = intent.getLongExtra("pos", 0);
                    long dur = intent.getLongExtra("dur", 0);
                    boolean play = intent.getBooleanExtra("play", false);
                    fab_status.setText(TorrentPlayer.formatHeader(MainActivity.this, pos, dur));
                    if (play)
                        fab_play.setImageResource(R.drawable.ic_pause_24dp);
                    else
                        fab_play.setImageResource(R.drawable.ic_play_arrow_black_24dp);
                }
                if (a.equals(TorrentPlayer.PLAYER_NEXT)) {
                    fab_panel.setVisibility(View.VISIBLE);
                    playerTorrent = intent.getLongExtra("t", -1);
                    long pos = intent.getLongExtra("pos", 0);
                    long dur = intent.getLongExtra("dur", 0);
                    boolean play = intent.getBooleanExtra("play", false);
                    if (play) {
                        fab_status.setText(TorrentPlayer.formatHeader(MainActivity.this, pos, dur));
                    } else { // next can point on non audio file
                        fab_status.setText("--");
                    }
                    fab_play.setImageResource(R.drawable.ic_pause_24dp);
                }
                if (a.equals(TorrentPlayer.PLAYER_STOP)) {
                    playerStop();
                }
            }
        };
    }

    void playerStop() {
        fab_panel.setVisibility(View.GONE);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(MainApplication.PREFERENCE_ANNOUNCE)) {
            Libtorrent.setDefaultAnnouncesList(sharedPreferences.getString(MainApplication.PREFERENCE_ANNOUNCE, ""));
        }
        if (key.equals(MainApplication.PREFERENCE_WIFI)) {
            if (!sharedPreferences.getBoolean(MainApplication.PREFERENCE_WIFI, true)) {
                storage.resume(); // wifi only disabled
            } else { // wifi only enabed
                if (!WifiReceiver.isConnectedWifi(this)) // are we on wifi?
                    storage.pause(); // no, pause all
            }
        }
        if (key.equals(MainApplication.PREFERENCE_SPEEDLIMIT)) {
            updateHeader(storage);
            storage.updateRates();
        }
        if (key.equals(MainApplication.PREFERENCE_UPLOAD) || key.equals(MainApplication.PREFERENCE_DOWNLOAD)) {
            storage.updateRates();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem login = menu.findItem(R.id.action_login);
        MenuItem home = menu.findItem(R.id.action_home);
        MenuItem grid = menu.findItem(R.id.action_grid);

        KeyguardManager myKM = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
        if (myKM.inKeyguardRestrictedInputMode() || delayedInit != null) {
            menu.findItem(R.id.action_settings).setVisible(false);
            menu.findItem(R.id.action_show_folder).setVisible(false);
        }

        MenuItem folder = menu.findItem(R.id.action_show_folder);
        folder.setVisible(false);
        Storage s = storage;
        if (s != null) {
            Intent intent = TorrentContentProvider.getProvider().openFolderIntent(this, s.getStoragePath()); // TorrentContentProvider.getStorageUri());
            if (MainActivity.isCallable(this, intent)) {
                folder.setVisible(true);
                folder.setIntent(intent);
            }
        }

        MenuItem search = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) search.getActionView();
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (lastSearch != null && !lastSearch.isEmpty())
                    searchView.setQuery(lastSearch, false);
            }
        });
        searchView.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                lastSearch = query;
                searchView.clearFocus();
                Adapter a = getAdapter();
                if (a != null && a instanceof MainActivity.NavigatorInterface) {
                    ((MainActivity.NavigatorInterface) a).search(query);
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        searchView.setOnCollapsedListener(new SearchView.OnCollapsedListener() {
            @Override
            public void onCollapsed() {
                Adapter a = getAdapter();
                if (a != null && a instanceof MainActivity.NavigatorInterface) {
                    ((MainActivity.NavigatorInterface) a).searchClose();
                }
            }
        });
        searchView.setOnCloseButtonListener(new SearchView.OnCloseButtonListener() {
            @Override
            public void onClosed() {
                lastSearch = "";
            }
        });

        home.setVisible(false);
        login.setVisible(false);
        grid.setVisible(false);

        ListAdapter a = getAdapter();
        if (a instanceof NavigatorInterface) {
            ((NavigatorInterface) a).onCreateOptionsMenu(menu);
        }

        return true;
    }

    public void close() {
        delayedInit = null; // prevent delayed delayedInit

        if (drawer != null) {
            drawer.close();
            drawer = null;
        }

        if (dialog != null) {
            dialog.close();
            dialog = null;
        }

        if (engies != null) {
            engies.save();
            engies.close();
            engies = null;
        }

        refreshUI = null;

        if (refresh != null) {
            handler.removeCallbacks(refresh);
            refresh = null;
        }

        if (torrents != null) {
            torrents.close();
            torrents = null;
        }

        if (storage != null) {
            storage.save();
            storage = null;
        }

        if (screenreceiver != null) {
            screenreceiver.close();
            screenreceiver = null;
        }

        if (playerReceiver != null) {
            playerReceiver.close();
            playerReceiver = null;
        }

        list.setAdapter(null); // remove torrent adapter so no storage calles from list

        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        shared.unregisterOnSharedPreferenceChangeListener(MainActivity.this);

        // do not close storage when mainactivity closes. it may be restarted due to theme change.
        // only close it on shutdown()
        // app.close();
    }

    public void shutdown() {
        close();
        getApp().close();
        if (Build.VERSION.SDK_INT >= 16)
            finishAffinity();
        else
            finish();
        ExitActivity.exitApplication(this);
    }

    public void post(final Throwable e) {
        Log.e(TAG, "Exception", e);
        Throwable t = e;
        while (t.getCause() != null)
            t = t.getCause();
        post(t.getClass().getCanonicalName() + ": " + t.getMessage());
    }

    public void post(final String msg) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (isFinishing())
                    return;
                Error(msg);
            }
        });
    }

    public AlertDialog Error(Throwable e) {
        Log.e(TAG, "Exception", e);
        Throwable t = e;
        while (t.getCause() != null)
            t = t.getCause();
        return Error(t.getClass().getCanonicalName() + ": " + t.getMessage());
    }

    public AlertDialog Error(String err) {
        Log.e(TAG, Libtorrent.error());
        return new AlertDialog.Builder(MainActivity.this)
                .setTitle(R.string.error)
                .setMessage(err)
                .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar base clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        if (id == R.id.action_shutdown) {
            shutdown();
            return true;
        }

        if (id == R.id.action_about) {
            AboutPreferenceCompat.showDialog(this, R.raw.about);
            return true;
        }

        if (id == R.id.action_show_folder) {
            Intent intent = item.getIntent();
            startActivity(intent);
            return true;
        }

        ListAdapter a = getAdapter();
        if (a instanceof NavigatorInterface) {
            return ((NavigatorInterface) a).onOptionsItemSelected(item);
        }

        return super.onOptionsItemSelected(item);
    }

    public Intent openFolderIntent(long t) {
        return TorrentContentProvider.openFolderIntent(this, storage.find(t));
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");

        if (isFinishing())
            return;

        invalidateOptionsMenu();

        // update if keyguard enabled or not
        drawer.updateManager();

        KeyguardManager myKM = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
        if (myKM.inKeyguardRestrictedInputMode()) {
            add.setVisibility(View.GONE);
            create.setVisibility(View.GONE);
        } else {
            if (Storage.permitted(this, Storage.PERMISSIONS_RW)) {
                add.setVisibility(View.VISIBLE);
                create.setVisibility(View.VISIBLE);
            }
        }

        migrateLocalStorage();

        refreshUI = new Runnable() {
            @Override
            public void run() {
                torrents.notifyDataSetChanged();

                if (dialog != null)
                    dialog.update();

                ListAdapter a = getAdapter();
                if (a instanceof TorrentFragmentInterface) {
                    ((TorrentFragmentInterface) a).update();
                }
            }
        };

        refresh = new Runnable() {
            @Override
            public void run() {
                handler.removeCallbacks(refresh);
                handler.postDelayed(refresh, AlarmManager.SEC1);

                if (delayedInit != null)
                    return;

                Storage s = storage;

                if (s == null) { // sholud never happens, unless if onResume called after shutdown()
                    return;
                }

                s.update();
                updateHeader(s);

                torrents.updateStorage();

                if (refreshUI != null)
                    refreshUI.run();
            }
        };
        refresh.run();

        ScreenlockPreference.onResume(this, MainApplication.PREFERENCE_SCREENLOCK);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        refreshUI = null;
        ScreenlockPreference.onPause(this, MainApplication.PREFERENCE_SCREENLOCK);
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        ScreenlockPreference.onUserInteraction(this, MainApplication.PREFERENCE_SCREENLOCK);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case RESULT_CREATE_TORRENT:
            case RESULT_ADD_TORRENT:
                choicer.onRequestPermissionsResult(permissions, grantResults);
                break;
            case RESULT_ADD_ENGINE:
                drawer.onRequestPermissionsResult(permissions, grantResults);
                break;
        }
    }

    void migrateLocalStorage() {
        try {
            if (storage == null)
                return;
            storage.migrateLocalStorage();
        } catch (RuntimeException e) {
            Error(e);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RESULT_ADD_TORRENT:
                choicer.onActivityResult(resultCode, data);
                break;
            case RESULT_ADD_ENGINE:
                drawer.onActivityResult(resultCode, data);
                break;
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        this.scrollState = scrollState;
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen()) {
            drawer.closeDrawer();
        } else {
            moveTaskToBack(true);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (delayedInit == null)
                    list.smoothScrollToPosition(torrents.getSelected());
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestory");
        close();
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
    }


    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    @Override
    public void onDismiss(DialogInterface dialogInterface) {
        if (dialogInterface instanceof AddDialogFragment.Result) {
            AddDialogFragment.Result r = (AddDialogFragment.Result) dialogInterface;
            if (r.ok) {
                Storage.Torrent t = storage.add(new Storage.Torrent(this, r.t, r.path, true));
                autoCheck(t);
                if (dialogInterface instanceof CreateDialogFragment.Result) { // created torrent marked as uncompleted, run checks
                    t.done = true;
                    t.created = true;
                } else {
                    t.done = t.completed(); // do not show notification for completed new added torrents
                }
                torrentUnread(t);
                updateUnread();
            } else {
                storage.cancelTorrent(r.hash);
            }
        }
        if (dialog != null)
            dialog.close();
        dialog = null;
        ListAdapter a = getAdapter();
        if (a instanceof DialogInterface.OnDismissListener) {
            ((DialogInterface.OnDismissListener) a).onDismiss(dialogInterface);
        }
    }

    void updateHeader(Storage s) {
        freespace.setText(s.formatHeader());

        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);
        if (shared.getBoolean(MainApplication.PREFERENCE_SPEEDLIMIT, false))
            turtle.setColorFilter(ThemeUtils.getColor(this, R.color.turtle));
        else
            turtle.setColorFilter(Color.GRAY);

        drawer.updateHeader();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        if (delayedInit == null) // no init pending?
            openIntent(intent);
        else
            delayedIntent = true;
    }

    void openIntent(Intent intent) {
        if (intent == null)
            return;

        String a = intent.getAction();
        if (a != null && a.equals(ACTION_SHUTDOWN)) {
            shutdown();
            return;
        }

        Uri openUri = intent.getData();
        if (openUri == null)
            return;

        OpenIntentDialogFragment dialog = new OpenIntentDialogFragment();

        Bundle args = new Bundle();
        args.putString("url", openUri.toString());

        dialog.setArguments(args);
        dialog.show(getSupportFragmentManager(), "");
    }

    public void addMagnet(String ff) {
        addMagnet(ff, null);
    }

    public void addMagnet(String ff, String url) {
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        addMagnet(ff, url, shared.getBoolean(MainApplication.PREFERENCE_DIALOG, false));
    }

    public void addMagnet(String ff, String url, boolean dialog) {
        try {
            List<String> m = Storage.splitMagnets(this, ff);

            if (dialog && m.size() == 1) {
                String s = m.get(0);

                if (engies.addManget(s))
                    return;

                Uri p = storage.getStoragePath();
                Storage.Torrent tt = storage.prepareTorrentFromMagnet(p, s);
                if (tt == null)
                    throw new RuntimeException(Libtorrent.error());
                if (url != null)
                    Libtorrent.torrentInfoComment(tt.t, url);

                addTorrentDialog(tt.t, p, tt.hash); // no hash, we need no file IO interface for magnet torrents
                return;
            } else {
                for (String s : m) {
                    if (!engies.addManget(s)) {
                        Storage.Torrent tt = storage.addMagnet(s);
                        if (url != null)
                            Libtorrent.torrentInfoComment(tt.t, url);
                        torrentUnread(tt);
                        Toast.makeText(MainActivity.this, getString(R.string.added) + " " + tt.name(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        } catch (RuntimeException e) {
            Error(e);
        }
        torrents.notifyDataSetChanged();
        updateUnread();
    }

    public Storage.Torrent addTorrentFromBytes(byte[] buf) {
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        return addTorrentFromBytes(storage.getStoragePath(), buf, shared.getBoolean(MainApplication.PREFERENCE_DIALOG, false));
    }

    public Storage.Torrent addTorrentFromBytes(Uri pp, byte[] buf, boolean dialog) {
        Storage.Torrent tt = null;
        try {
            if (dialog) {
                tt = storage.prepareTorrentFromBytes(pp, buf);
                if (tt == null) {
                    throw new RuntimeException(Libtorrent.error());
                }
                addTorrentDialog(tt.t, pp, tt.hash);
            } else {
                tt = storage.addTorrentFromBytes(buf);
                torrentUnread(tt);
                Toast.makeText(MainActivity.this, getString(R.string.added) + " " + tt.name(), Toast.LENGTH_SHORT).show();
            }
        } catch (RuntimeException e) {
            Error(e);
        }
        torrents.notifyDataSetChanged();
        updateUnread();
        return tt;
    }

    void addTorrentDialog(long t, Uri path, String hash) {
        AddDialogFragment fragment = new AddDialogFragment();

        dialog = fragment;

        Bundle args = new Bundle();
        args.putLong("torrent", t);
        args.putString("hash", hash);
        args.putString("path", path.toString());

        fragment.setArguments(args);

        fragment.show(getSupportFragmentManager(), "");
    }

    void createTorrentDialog(long t, Uri path, String hash) {
        CreateDialogFragment fragment = new CreateDialogFragment();

        dialog = fragment;

        Bundle args = new Bundle();
        args.putLong("torrent", t);
        args.putString("path", path.toString());
        args.putString("hash", hash);

        fragment.setArguments(args);

        fragment.show(getSupportFragmentManager(), "");
    }

    public void createTorrent(final Uri pp, Uri u) {
        final ProgressDialog progress = new ProgressDialog(MainActivity.this);
        progress.setOwnerActivity(MainActivity.this);
        progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

        final MetainfoBuilder b = new MetainfoBuilder(pp, storage, u);
        final AtomicLong pieces = new AtomicLong(Libtorrent.createMetainfoBuilder(b));
        if (pieces.get() == -1) {
            Error(Libtorrent.error());
            return;
        }
        final AtomicLong i = new AtomicLong(0);
        progress.setMax((int) pieces.get());

        final Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                final MainActivity activity = MainActivity.this;

                for (i.set(0); i.get() < pieces.get(); i.incrementAndGet()) {
                    Thread.yield();

                    if (Thread.currentThread().isInterrupted()) {
                        Libtorrent.closeMetaInfo();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (activity.isFinishing())
                                    return;
                                progress.dismiss();
                            }
                        });
                        return;
                    }

                    if (!Libtorrent.hashMetaInfo(i.get())) {
                        activity.post(Libtorrent.error());
                        Libtorrent.closeMetaInfo();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (activity.isFinishing())
                                    return;
                                progress.dismiss();
                            }
                        });
                        return;
                    }
                }

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (activity.isFinishing())
                            return;
                        MainActivity.this.dialog = null;
                        activity.createTorrentFromMetaInfo(Uri.parse(b.root()));
                        Libtorrent.closeMetaInfo();
                        progress.dismiss();
                    }
                });
            }
        }, "Create Torrent Thread");

        if (MainActivity.this.dialog != null)
            MainActivity.this.dialog.close();
        MainActivity.this.dialog = new TorrentFragmentInterface() {
            @Override
            public void update() {
                progress.setProgress((int) i.get());
            }

            @Override
            public void close() {
                t.interrupt();
            }
        };

        progress.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                t.interrupt();
            }
        });
        progress.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                t.start();
            }
        });
        progress.show();
    }

    public void createTorrentFromMetaInfo(Uri pp) {
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        Storage.Torrent tt = storage.prepareTorrentFromBuilder(pp);
        if (tt == null) {
            Error(Libtorrent.error());
            return;
        }
        tt.done = true;
        tt.created = true;
        autoCheck(tt);
        if (shared.getBoolean(MainApplication.PREFERENCE_DIALOG, false)) {
            createTorrentDialog(tt.t, pp, tt.hash);
        } else {
            storage.add(tt);
            torrentUnread(tt);
            torrents.notifyDataSetChanged();
            updateUnread();
        }
    }

    void autoCheck(Storage.Torrent tt) {
        if (!Libtorrent.metaTorrent(tt.t))
            return;
        if (!tt.completed() && !tt.isChecking()) {
            String s = tt.path.getScheme();
            if (s.equals(ContentResolver.SCHEME_FILE)) {
                File f = new File(tt.path.getPath());
                if (f.exists()) {
                    tt.check();
                }
            }
        }
    }

    public boolean active(ListAdapter s) {
        Adapter a = getAdapter();
        return s == a;
    }

    public void updateUnread() {
        if (engies == null)
            return; // we got error after exit
        drawer.updateUnread();
        drawer.updateManager();
    }

    void torrentUnread(Storage.Torrent tt) {
        if (active(torrents)) {
            tt.message = false;
        }
    }

    public Torrents getTorrents() {
        return torrents;
    }

    public EnginesManager getEngines() {
        return engies;
    }

    public void show(NavigatorInterface nav) {
        Adapter a = getAdapter();
        if (a != null && a instanceof MainActivity.NavigatorInterface) {
            ((MainActivity.NavigatorInterface) a).remove(list);
        }

        empty.setVisibility(View.GONE);

        if (nav instanceof Torrents) {
            list.setEmptyView(empty);
        } else {
            list.setEmptyView(null);
        }

        nav.install(list);

        updateListPadding();
    }

    public void updateListPadding() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                int[] cc = new int[2];
                list.getLocationOnScreen(cc);
                View v = fab.findViewById(R.id.fab_expand_menu_button);
                int[] tt = new int[2];
                v.getLocationOnScreen(tt);
                int p = (cc[1] + list.getHeight()) - tt[1];
                list.setPadding(list.getPaddingLeft(), list.getPaddingTop(), list.getPaddingRight(), p);
            }
        });
    }

    public Drawer getDrawer() {
        return drawer;
    }

    public void showRates() {
        RatesDialogFragment dialog = new RatesDialogFragment();
        Bundle args = new Bundle();
        dialog.setArguments(args);
        dialog.show(getSupportFragmentManager(), "");
    }

    public void openTorrents() {
        if (torrents == null)
            return; // delayed init
        show(torrents);
    }

}
