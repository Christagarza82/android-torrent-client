package com.github.axet.torrentclient.app;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

import com.github.axet.androidlibrary.app.AlarmManager;
import com.github.axet.androidlibrary.widgets.ProximityPlayer;
import com.github.axet.torrentclient.R;
import com.github.axet.torrentclient.activities.PlayerActivity;
import com.github.axet.torrentclient.services.TorrentContentProvider;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.UnrecognizedInputFormatException;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import net.lingala.zip4j.core.NativeStorage;
import net.lingala.zip4j.core.ZipFile;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.innosystec.unrar.Archive;
import de.innosystec.unrar.exception.RarException;
import de.innosystec.unrar.rarfile.FileHeader;
import libtorrent.Libtorrent;

public class TorrentPlayer {
    public static String TAG = TorrentPlayer.class.getSimpleName();

    public static final String PLAYER_NEXT = TorrentPlayer.class.getCanonicalName() + ".PLAYER_NEXT";
    public static final String PLAYER_PROGRESS = TorrentPlayer.class.getCanonicalName() + ".PLAYER_PROGRESS";
    public static final String PLAYER_STOP = TorrentPlayer.class.getCanonicalName() + ".PLAYER_STOP";
    public static final String PLAYER_PAUSE = TorrentPlayer.class.getCanonicalName() + ".PLAYER_PAUSE";

    public static final String CONTENTTYPE_PDF = "application/pdf";

    Context context;
    Storage.Torrent torrent;
    ArrayList<PlayerFile> files = new ArrayList<>();
    public String torrentHash;
    public String torrentName;
    Storage storage;
    SimpleExoPlayer player;
    int playingIndex = -1;
    Uri playingUri;
    PlayerFile playingFile;
    Runnable next;
    Runnable progress = new Runnable() {
        @Override
        public void run() {
            notifyProgress();
            handler.removeCallbacks(progress);
            handler.postDelayed(progress, AlarmManager.SEC1);
        }
    };
    Runnable saveDelay = new Runnable() {
        @Override
        public void run() {
            save(context, TorrentPlayer.this);
            saveDelay();
        }
    };
    Handler handler = new Handler(Looper.getMainLooper());
    int video = -1; // index
    ProximityPlayer proximity;

    Runnable receiver;

    public Decoder RAR = new Decoder() {
        ArrayList<Archive> aa = new ArrayList<>();

        @Override
        public boolean supported(TorFile f) {
            Uri u = storage.child(torrent.path, f.file.getPath());
            String s = u.getScheme();
            if (s.startsWith(ContentResolver.SCHEME_CONTENT)) {
                return u.getPath().endsWith(".rar");
            } else if (s.startsWith(ContentResolver.SCHEME_FILE)) {
                File local = new File(u.getPath());
                if (!local.exists())
                    return false;
                if (!local.isFile())
                    return false;
                return local.getName().toLowerCase().endsWith(".rar");
            } else {
                throw new Storage.UnknownUri();
            }
        }

        @Override
        public ArrayList<ArchiveFile> list(TorFile f) {
            try {
                ArrayList<ArchiveFile> ff = new ArrayList<>();
                Uri u = storage.child(torrent.path, f.file.getPath());
                String s = u.getScheme();
                final Archive archive;
                if (s.startsWith(ContentResolver.SCHEME_CONTENT)) {
                    RarNativeStorageSAF local = new RarNativeStorageSAF(storage, torrent.path, u);
                    archive = new Archive(new RarNativeStorageSAF(local));
                } else if (s.startsWith(ContentResolver.SCHEME_FILE)) {
                    File local = new File(u.getPath());
                    archive = new Archive(new de.innosystec.unrar.NativeStorage(local));
                } else {
                    throw new Storage.UnknownUri();
                }
                List<FileHeader> list = archive.getFileHeaders();
                for (FileHeader h : list) {
                    if (h.isDirectory())
                        continue;
                    final FileHeader header = h;
                    ArchiveFile a = new ArchiveFile() {
                        @Override
                        public String getPath() {
                            String s = header.getFileNameW();
                            if (s == null || s.isEmpty())
                                s = header.getFileNameString();
                            return s;
                        }

                        @Override
                        public InputStream open() {
                            try {
                                final PipedInputStream is = new PipedInputStream();
                                final PipedOutputStream os = new PipedOutputStream(is);
                                Thread thread = new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            archive.extractFile(header, os);
                                            os.flush();
                                            os.close();
                                        } catch (Exception e) {
                                            throw new RuntimeException(e);
                                        }
                                    }
                                }, "Write Archive File");
                                thread.start();
                                return is;
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        }

                        @Override
                        public void copy(OutputStream os) {
                            try {
                                archive.extractFile(header, os);
                            } catch (RarException e) {
                                throw new RuntimeException(e);
                            }
                        }

                        @Override
                        public long getLength() {
                            return header.getFullUnpackSize();
                        }
                    };
                    ff.add(a);
                }
                return ff;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public void clear() {
            try {
                for (Archive a : aa) {
                    a.close();
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            aa.clear();
        }
    };

    public Decoder ZIP = new Decoder() {
        ArrayList<ZipFile> aa = new ArrayList<>();

        @Override
        public boolean supported(TorFile f) {
            Uri u = storage.child(torrent.path, f.file.getPath());
            String s = u.getScheme();
            if (s.startsWith(ContentResolver.SCHEME_CONTENT)) {
                return u.getPath().endsWith(".zip");
            } else if (s.startsWith(ContentResolver.SCHEME_FILE)) {
                File local = new File(u.getPath());
                if (!local.exists())
                    return false;
                if (!local.isFile())
                    return false;
                return local.getName().toLowerCase().endsWith(".zip");
            } else {
                throw new Storage.UnknownUri();
            }
        }

        @Override
        public ArrayList<ArchiveFile> list(TorFile f) {
            try {
                ArrayList<ArchiveFile> ff = new ArrayList<>();
                Uri u = storage.child(torrent.path, f.file.getPath());
                String s = u.getScheme();
                final ZipFile zip;
                if (s.startsWith(ContentResolver.SCHEME_CONTENT)) {
                    zip = new ZipFile(new ZipNativeStorageSAF(storage, torrent.path, u));
                } else if (s.startsWith(ContentResolver.SCHEME_FILE)) {
                    File local = new File(u.getPath());
                    zip = new ZipFile(new NativeStorage(local));
                } else {
                    throw new Storage.UnknownUri();
                }
                aa.add(zip);
                List list = zip.getFileHeaders();
                for (Object o : list) {
                    final net.lingala.zip4j.model.FileHeader zipEntry = (net.lingala.zip4j.model.FileHeader) o;
                    if (zipEntry.isDirectory())
                        continue;
                    ArchiveFile a = new ArchiveFile() {

                        @Override
                        public String getPath() {
                            return zipEntry.getFileName();
                        }

                        @Override
                        public InputStream open() {
                            try {
                                return zip.getInputStream(zipEntry);
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        }

                        public void copy(OutputStream os) {
                            try {
                                InputStream is = zip.getInputStream(zipEntry);
                                IOUtils.copy(is, os);
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        }

                        @Override
                        public long getLength() {
                            return zipEntry.getUncompressedSize();
                        }
                    };
                    ff.add(a);
                }
                return ff;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public void clear() {
            aa.clear();
        }
    };

    Decoder[] DECODERS = new Decoder[]{RAR, ZIP};

    public static String getType(PlayerFile f) {
        return Storage.getTypeByName(f.getName());
    }

    public static boolean isVideo(String type) {
        if (type != null)
            return type.startsWith("video");
        return false;
    }

    public static boolean isSupported(String type) {
        if (type != null && !type.isEmpty()) {
            String[] skip = new String[]{"image/", "text/", CONTENTTYPE_PDF}; // MediaPlayer will open jpg and wait forever
            for (String s : skip) {
                if (type.startsWith(s))
                    return false;
            }
            String[] support = new String[]{"audio/", "video/"};
            for (String s : support) {
                if (type.startsWith(s))
                    return true;
            }
        }
        return true; // make default true
    }

    public static void save(Context context, TorrentPlayer player) {
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = shared.edit();
        save(player, edit);
        edit.commit();
    }

    public static void save(TorrentPlayer player, SharedPreferences.Editor edit) {
        if (player != null) {
            Uri uri = player.getStateUri();
            if (uri != null) {
                edit.putString(MainApplication.PREFERENCE_PLAYER, uri.toString());
                return;
            }
        }
        edit.remove(MainApplication.PREFERENCE_PLAYER);
    }

    public static String formatHeader(Context context, long pos, long dur) {
        String header = MainApplication.formatDuration(context, pos);
        if (dur > 0)
            header += "/" + MainApplication.formatDuration(context, dur);
        return header;
    }

    public static class ExoMediaPlayer extends SimpleExoPlayer { // ExoPlayer does not support some old (AMR...) formats
        Context context;
        android.media.MediaPlayer player;
        Uri u;
        Player.EventListener listener;
        Handler handler = new Handler(Looper.getMainLooper());
        android.media.AudioAttributes aa;
        int streamType = -1; // AudioSystem.STREAM_DEFAULT

        public ExoMediaPlayer(Context context, TrackSelector trackSelector, Uri u) {
            super(new DefaultRenderersFactory(context), trackSelector, new DefaultLoadControl());
            this.context = context;
            this.u = u;
        }

        @Override
        public void release() {
            super.release();
            if (player != null)
                player.release();
        }

        public android.media.MediaPlayer prepare(Uri uri) {
            android.media.MediaPlayer mp = new android.media.MediaPlayer();
            if (Build.VERSION.SDK_INT >= 21) {
                final android.media.AudioAttributes a = aa != null ? aa : new android.media.AudioAttributes.Builder().build();
                mp.setAudioAttributes(a);
            } else {
                if (streamType != -1) {
                    mp.setAudioStreamType(streamType);
                }
            }
            try {
                mp.setDataSource(context, uri);
                mp.prepare();
                return mp;
            } catch (IOException e) {
                Log.d(TAG, "unable to create MediaPlayer", e);
                return null;
            }
        }

        @Override
        public void prepare(MediaSource mediaSource) {
            player = prepare(u);
            if (player == null) {
                handler.post(new Runnable() { // prepare should not call onPlayerError instantly
                    @Override
                    public void run() {
                        if (listener != null)
                            listener.onPlayerError(ExoPlaybackException.createForSource(new IOException("failed to open")));
                    }
                });
                return;
            }
            player.setOnErrorListener(new android.media.MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(android.media.MediaPlayer mp, int what, int extra) {
                    if (listener != null)
                        listener.onPlayerError(ExoPlaybackException.createForSource(new IOException(what + " " + extra)));
                    return false; // OnCompletionListener called
                }
            });
            player.setOnCompletionListener(new android.media.MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(android.media.MediaPlayer mp) {
                    if (listener != null)
                        listener.onPlayerStateChanged(true, Player.STATE_ENDED);
                }
            });
        }

        @Override
        public long getCurrentPosition() {
            if (player == null)
                return 0;
            return player.getCurrentPosition();
        }

        @Override
        public void setPlayWhenReady(boolean playWhenReady) {
            if (player == null)
                return;
            if (!playWhenReady) {
                if (player.isPlaying())
                    player.pause();
            } else {
                if (!player.isPlaying())
                    player.start();
            }
        }

        @Override
        public boolean getPlayWhenReady() {
            if (player == null)
                return false;
            return player.isPlaying();
        }

        @Override
        public long getDuration() {
            if (player == null)
                return 0;
            return player.getDuration();
        }

        @Override
        public void seekTo(long positionMs) {
            if (player == null)
                return;
            player.seekTo((int) positionMs);
        }

        @Override
        public void addListener(Player.EventListener listener) {
            this.listener = listener;
        }

        @TargetApi(21)
        @Override
        public void setAudioAttributes(AudioAttributes audioAttributes) { // ExoPlayer can call setAudioAttributes while playing, MediaPlayer can't
            android.media.AudioAttributes.Builder b = new android.media.AudioAttributes.Builder();
            b.setUsage(audioAttributes.usage);
            b.setFlags(audioAttributes.flags);
            b.setContentType(audioAttributes.contentType);
            aa = b.build();
            if (player != null) {
                int pos = player.getCurrentPosition();
                player.release();
                prepare((MediaSource) null);
                player.seekTo(pos);
                player.start();
            }
        }

        @Override
        public void setAudioStreamType(int streamType) {
            this.streamType = streamType;
            if (player != null) {
                int pos = player.getCurrentPosition();
                player.release();
                prepare((MediaSource) null);
                player.seekTo(pos);
                player.start();
            }
        }
    }

    public static class EventListener implements Player.EventListener {
        @Override
        public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {
        }

        @Override
        public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
        }

        @Override
        public void onLoadingChanged(boolean isLoading) {
        }

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        }

        @Override
        public void onRepeatModeChanged(int repeatMode) {
        }

        @Override
        public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {
        }

        @Override
        public void onPlayerError(ExoPlaybackException error) {
        }

        @Override
        public void onPositionDiscontinuity(int reason) {
        }

        @Override
        public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
        }

        @Override
        public void onSeekProcessed() {
        }
    }

    public static class State {
        public Uri state;
        public Uri uri;
        public String hash;
        public long t;

        public State(String u) {
            this(Uri.parse(u));
        }

        public State(Uri u) {
            state = u;
            String p = u.getPath();
            String[] pp = p.split("/");
            hash = pp[1];
            String v = u.getQueryParameter("t");
            if (v != null && !v.isEmpty())
                t = Integer.parseInt(v);
            Uri.Builder b = u.buildUpon();
            b.clearQuery();
            uri = b.build();
        }
    }

    public static int compareFiles(String f1, String f2) { // can point to files only, no folders
        List<String> s1 = MainApplication.splitPath(f1);
        List<String> s2 = MainApplication.splitPath(f2);

        int c = Integer.valueOf(s1.size()).compareTo(s2.size());
        if (c != 0) { // not same folder, compare only parent folders
            s1.remove(s1.size() - 1);
            s2.remove(s2.size() - 1);
        }

        int m = Math.min(s1.size(), s2.size());
        for (int i = 0; i < m; i++) {
            String p1 = s1.get(i);
            String p2 = s2.get(i);
            int c2 = p1.compareTo(p2);
            if (c2 != 0)
                return c2;
        }

        return c;
    }

    public static class SortPlayerFiles implements Comparator<PlayerFile> {
        @Override
        public int compare(TorrentPlayer.PlayerFile file, TorrentPlayer.PlayerFile file2) {
            return compareFiles(file.getPath(), file2.getPath());
        }
    }

    public static class SortArchiveFiles implements Comparator<ArchiveFile> {
        @Override
        public int compare(ArchiveFile file, ArchiveFile file2) {
            return compareFiles(file.getPath(), file2.getPath());
        }
    }

    public static class Receiver extends BroadcastReceiver {
        Context context;

        public Receiver(Context context) {
            this.context = context;
            IntentFilter ff = new IntentFilter();
            ff.addAction(PLAYER_NEXT);
            ff.addAction(PLAYER_PROGRESS);
            ff.addAction(PLAYER_STOP);
            context.registerReceiver(this, ff);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
        }

        public void close() {
            context.unregisterReceiver(this);
        }
    }


    public interface Decoder {
        boolean supported(TorFile f);

        ArrayList<ArchiveFile> list(TorFile f);

        void clear();
    }

    public interface ArchiveFile {
        String getPath();

        InputStream open();

        void copy(OutputStream os);

        long getLength();
    }

    public static class TorFile {
        public long index; // libtorrent index
        public libtorrent.File file;

        public TorFile(long i, libtorrent.File f) {
            this.index = i;
            this.file = f;
        }
    }

    public class PlayerFile {
        public int index; // file index in dirrectory / archive
        public int count; // directory count
        public Uri uri;
        public TorFile tor;
        public ArchiveFile arch;

        public PlayerFile(TorFile t) {
            this.tor = t;
            uri = TorrentContentProvider.getUriForFile(torrentHash, t.file.getPath());
        }

        public PlayerFile(TorFile t, ArchiveFile f) {
            this.tor = t;
            this.arch = f;
            File ff = new File(tor.file.getPath(), arch.getPath());
            uri = TorrentContentProvider.getUriForFile(torrentHash, ff.getPath());
        }

        public PlayerFile index(int i, int count) {
            this.index = i;
            this.count = count;
            return this;
        }

        public Uri getFile() {
            if (arch != null) { // archive file
                return storage.child(torrent.path, arch.getPath());
            } else { // local file
                return storage.child(torrent.path, tor.file.getPath());
            }
        }

        public String getPath() {
            if (arch != null) {
                return new File(tor.file.getPath(), arch.getPath()).toString();
            }
            return tor.file.getPath();
        }

        public String getName() {
            if (arch != null) {
                return new File(arch.getPath()).getName();
            }
            return new File(tor.file.getPath()).getName();
        }

        public long getLength() {
            if (arch != null)
                return arch.getLength();
            return tor.file.getLength();
        }

        public boolean isLoaded() {
            return tor.file.getBytesCompleted() == tor.file.getLength();
        }

        public int getPercent() {
            return (int) (tor.file.getBytesCompleted() * 100 / tor.file.getLength());
        }

        public String toString() {
            return getName();
        }
    }

    public TorrentPlayer(Context context, Storage storage, long t) {
        this(context, storage, storage.find(t));
    }

    public TorrentPlayer(final Context context, Storage storage, Storage.Torrent t) {
        this.context = context;
        this.storage = storage;
        this.torrent = t;

        receiver = new BroadcastReceiver() {
            BroadcastReceiver that = this;

            Runnable close = new Runnable() {
                @Override
                public void run() {
                    context.unregisterReceiver(that);
                }
            };

            {
                IntentFilter ff = new IntentFilter();
                ff.addAction(PLAYER_PAUSE);
                context.registerReceiver(that, ff);
            }

            @Override
            public void onReceive(Context context, Intent intent) {
                String a = intent.getAction();
                if (a.equals(PLAYER_PAUSE)) {
                    pause();
                }
            }
        }.close;

        update();
    }

    public void setContext(Context context) {
        this.context = context;
        if (proximity != null)
            proximity.setContext(context);
    }

    public Decoder getDecoder(TorFile f) {
        for (Decoder d : DECODERS) {
            if (d.supported(f))
                return d;
        }
        return null;
    }

    public void update() {
        Uri old = playingUri;

        torrentName = torrent.name();
        torrentHash = torrent.hash;

        long l = Libtorrent.torrentFilesCount(torrent.t);

        files.clear();
        ArrayList<PlayerFile> ff = new ArrayList<>();
        for (long i = 0; i < l; i++) {
            TorFile f = new TorFile(i, Libtorrent.torrentFiles(torrent.t, i));
            if (f.file.getCheck())
                ff.add(new PlayerFile(f).index((int) i, (int) l));
        }
        Collections.sort(ff, new SortPlayerFiles());

        for (Decoder d : DECODERS) {
            d.clear();
        }

        for (PlayerFile f : ff) {
            files.add(f);
            if (f.tor.file.getBytesCompleted() == f.tor.file.getLength()) {
                Decoder d = getDecoder(f.tor);
                if (d != null) {
                    try {
                        ArrayList<ArchiveFile> list = d.list(f.tor);
                        Collections.sort(list, new SortArchiveFiles());
                        int q = 0;
                        for (ArchiveFile a : list) {
                            files.add(new PlayerFile(f.tor, a).index(q++, list.size()));
                        }
                    } catch (RuntimeException e) {
                        Log.d(TAG, "Unable to unpack zip", e);
                    }
                }
            }
        }

        if (old == null)
            return;
        TorrentPlayer.PlayerFile f = find(old);
        playingIndex = -1;
        if (f == null)
            return;
        playingIndex = files.indexOf(f);
    }

    public int getSize() {
        return files.size();
    }

    public int getPlaying() {
        return playingIndex;
    }

    public PlayerFile get(int i) {
        return files.get(i);
    }

    public PlayerFile find(Uri uri) {
        for (PlayerFile f : files) {
            if (f.uri.equals(uri)) {
                return f;
            }
        }
        return null;
    }

    public boolean open(Uri uri) {
        TorrentPlayer.PlayerFile f = find(uri);
        if (f == null)
            return false;
        return open(f);
    }

    public boolean open(PlayerFile f) {
        Log.d(TAG, "open " + f);
        final int i = files.indexOf(f);
        if (player != null) {
            player.release();
            player = null;
            video = -1;
        }
        playingIndex = i;
        playingUri = f.uri;
        playingFile = f;
        if (f.tor.file.getBytesCompleted() == f.tor.file.getLength()) {
            if (isSupported(TorrentPlayer.getType(f))) {
                prepare(f.uri);
            }
        }
        if (player == null) {
            next(i + 1);
            return false;
        }
        return true;
    }

    public void play(final int i) {
        handler.removeCallbacks(next);
        PlayerFile f = get(i);
        if (!open(f))
            return;
        play();
    }

    public void play() {
        if (video != getPlaying()) { // already playing video? just call start()
            String type = getType(playingFile);
            if (isVideo(type)) {
                PlayerActivity.startActivity(context);
                return;
            } else {
                if (video >= 0) {
                    PlayerActivity.closeActivity(context);
                    video = -1;
                }
            }
        }
        resume();
    }

    public void close(PlayerView view) {
        if (isPlayingSound())
            pause();
        video = -1; // do not close player, keep seek progress
    }

    public void hide(PlayerView view) {
        video = -1; // do not close player, keep seek progress
    }

    void prepare(final Uri u) {
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        final TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
        player = ExoPlayerFactory.newSimpleInstance(context, trackSelector);
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(context, Util.getUserAgent(context, context.getString(R.string.app_name)));
        ExtractorMediaSource.Factory factory = new ExtractorMediaSource.Factory(dataSourceFactory);
        final MediaSource source = factory.createMediaSource(u);
        EventListener e = new EventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playbackState == Player.STATE_READY) {
                    ; // getDuration();
                }
                if (playbackState == Player.STATE_ENDED)
                    next(playingIndex + 1);
            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                Log.d(TAG, "Playing error", error);
                Exception e = error.getSourceException();
                if (e != null && e instanceof UnrecognizedInputFormatException) {
                    boolean p = player.getPlayWhenReady();
                    player.release();
                    player = new ExoMediaPlayer(context, trackSelector, u);
                    player.addListener(new EventListener() {
                        @Override
                        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                            if (playbackState == Player.STATE_ENDED)
                                next(playingIndex + 1);
                        }

                        @Override
                        public void onPlayerError(ExoPlaybackException error) {
                            Log.d(TAG, "Playing error", error);
                            next(playingIndex + 1);
                        }
                    });
                    player.prepare(null);
                    player.setPlayWhenReady(p);
                    return;
                }
                next(playingIndex + 1);
            }
        };
        player.addListener(e);
        player.prepare(source);
    }

    public void play(PlayerView view) {
        video = playingIndex;
        Long seek = null;
        if (player != null) {
            seek = player.getCurrentPosition();
            player.release();
        }
        prepare(playingUri);
        view.setPlayer(player);
        if (seek != null)
            player.seekTo(seek);
        resume();
    }

    public void resume() {
        if (proximity != null)
            proximity.close();
        proximity = new ProximityPlayer(context) {
            @Override
            public void prepare() {
                if (Build.VERSION.SDK_INT >= 21) {
                    @C.AudioUsage int usage = Util.getAudioUsageForStreamType(streamType);
                    @C.AudioContentType int contentType = Util.getAudioContentTypeForStreamType(streamType);
                    AudioAttributes audioAttributes = new AudioAttributes.Builder().setUsage(usage).setContentType(contentType).build();
                    player.setAudioAttributes(audioAttributes);
                } else {
                    player.setAudioStreamType(streamType); // to support ExoMediaPlayer API21<
                }
            }
        };
        proximity.create();
        player.setPlayWhenReady(true);
        progress.run();
        saveDelay();
    }

    public void next(final int next) {
        if (player != null) { // next on error or end
            player.release();
            player = null;
        }
        if (proximity != null) {
            proximity.close();
            proximity = null;
        }
        handler.removeCallbacks(this.saveDelay);
        handler.removeCallbacks(this.progress);
        handler.removeCallbacks(this.next);
        this.next = new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "next " + next);
                TorrentPlayer.this.next = null;
                if (next >= files.size()) {
                    stop();
                    notifyStop();
                    return; // n = 0;
                }
                PlayerFile f = get(next);
                boolean b = open(f);
                notifyNext();
                if (b)
                    play();
            }
        };
        handler.postDelayed(this.next, AlarmManager.SEC1);
    }

    public boolean isPlaying() { // actual playing mode (next or actual sound)
        if (next != null)
            return true;
        return isPlayingSound();
    }

    public boolean isPlayingSound() { // actual playing mode (actual sound)
        if (player == null)
            return false;
        return player.getPlayWhenReady();
    }

    public boolean isStop() {
        return player == null;
    }

    public void pause() {
        if (proximity != null) {
            proximity.close();
            proximity = null;
        }
        if (getPlaying() != -1) {
            if (player == null) {
                stop(); // clear next
                notifyStop();
                return;
            }
            if (isPlayingSound()) {
                player.setPlayWhenReady(false);
                notifyProgress();
                handler.removeCallbacks(progress);
                handler.removeCallbacks(saveDelay);
            } else {
                play();
            }
        }
    }

    public void stop() {
        Log.d(TAG, "stop");
        if (proximity != null) {
            proximity.close();
            proximity = null;
        }
        if (player != null) {
            player.setPlayWhenReady(false);
        }
        handler.removeCallbacks(progress);
        handler.removeCallbacks(next);
        handler.removeCallbacks(saveDelay);
        next = null;
        playingIndex = -1;
        playingUri = null;
        playingFile = null;
    }

    public void close() {
        stop();
        if (player != null) {
            player.release();
            player = null;
        }
        if (receiver != null) {
            receiver.run();
            receiver = null;
        }
    }

    public long getTorrent() {
        return torrent.t;
    }

    public long getDuration() {
        long d = player.getDuration();
        if (d == C.TIME_UNSET)
            return 0;
        return d;
    }

    Intent notify(String a) {
        Intent intent = new Intent(a);
        intent.putExtra("t", torrent.t);
        if (player != null) {
            intent.putExtra("pos", player.getCurrentPosition());
            intent.putExtra("dur", getDuration());
        }
        return intent;
    }

    public void notifyNext() {
        Intent intent = notify(PLAYER_NEXT);
        intent.putExtra("play", player != null);
        context.sendBroadcast(intent);
    }

    Intent notifyProgressIntent() {
        Intent intent = notify(PLAYER_PROGRESS);
        if (player != null)
            intent.putExtra("play", isPlaying() || next != null);
        else
            intent.putExtra("play", false);
        return intent;
    }

    public void notifyProgress() {
        Intent intent = notifyProgressIntent();
        context.sendBroadcast(intent);
    }

    public void notifyProgress(Receiver receiver) {
        Intent intent = notifyProgressIntent();
        receiver.onReceive(context, intent);
    }

    public void notifyStop() {
        Intent intent = new Intent(PLAYER_STOP);
        context.sendBroadcast(intent);
    }

    public Uri getStateUri() {
        if (player == null)
            return null;
        if (playingUri == null)
            return null;
        Uri.Builder b = playingUri.buildUpon();
        b.appendQueryParameter("t", "" + player.getCurrentPosition());
        return b.build();
    }

    public void seek(long i) {
        player.seekTo((int) i);
    }

    public String formatHeader() {
        if (player == null)
            return "";
        return formatHeader(context, player.getCurrentPosition(), getDuration());
    }

    public void saveDelay() {
        handler.removeCallbacks(saveDelay);
        handler.postDelayed(saveDelay, AlarmManager.MIN1);
    }
}
